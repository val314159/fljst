// fljst: Copyright (c) 2014 Joel "val" Ward (val@circleclick.com)
G={};
//function jeval(x){return JSON.parse('('+x+')');}
function jeval(x){return eval('('+x+')');}
function logtoggle() { E('out').hidden=!E('out').hidden; }
function undef(x){return x===undefined;}
function str(x){try{return JSON.stringify(x);}catch(e){return x;}}
function E(id) {return document.getElementById(id);}
function B(){return document.body;}
function mkE(tag,html,parent,parent2) {
    var elt=document.createElement(tag);
    elt.innerHTML=html;
    if (parent) parent.appendChild(elt);
    if (parent2) {
	parent2.insertBefore(elt,parent2.firstChild);}
    return elt;}
function out(m){mkE("DIV","<li>"+m+"</li>",E('out'));}
function clr(elt){(elt||E('out')).innerHTML='';}
function clrBody(){B().innerHTML="";}
function wrap(f){
    try{out("WRAP STARTED:"+f);
	if (typeof(f)=="function") {f();}
	else {eval(f);}
	out("WRAP SUCCESS:"+f);
    }catch(e){
	//clr();
	out("<font color=red>WRAP FAILURE:"+f+"::"+e+"</font>");}}
function main(){wrap("_main()");}
