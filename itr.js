// fljst: Copyright (c) 2014 Joel "val" Ward (val@circleclick.com)
function ArrayItr(arr) {
    var n=0;
    return {type:"ArrayItr",
next:function() {
	    return (n<arr.length
		    ? {value:arr[n++]}
		    : {done:true})}}}
function KeyFilt0(it,re) {
    return {
type:"KeyFilt0",
next:function() {
	    while (1) {
		var cur=it.next();
		if (cur.done) return cur;
		if (!re.exec(cur.value[0])) continue;
		return cur;}}}}
function KeyFilt1(it,re) {
    return {next:function() {
	    var cur={};
	    while (iterate(it.next(),cur)) {
		if (re.exec(cur.value[0])) continue;
		return cur.orig;
	    }
	    return cur.orig;}}}
function KeyFilt(it,re) {
    return {next:function() {
	    var cur=it.next();
	    if (cur.done) {return cur;}
	    if (re.exec(cur.value[0])) return {skip:1};
	    return cur;}}}
function dumpItr(it){
    while (1) {
	var res = it.next();
	if (res.done) break;
	out("RES:"+str(res.value));}}
function listify(it){
    var arr=[];
    while (1) {
	var res = it.next();
	if (res.done) return arr;
	else arr.push(res.value);}}
function iterate(rec,ret) {
    if (rec.done) return false;
    ret.value = rec.value;
    ret.orig = rec;
    return true;}
