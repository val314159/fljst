PROG=fljst
FILES=core.js itr.js ajax.js

all: fljst.js fljst_min.js
	@echo
	@echo SUCCESS: Created $?

fljst.js: $(FILES)
	cat $(FILES) >$(PROG).js

fljst_min.js: fljst.js
	jsmin <$(PROG).js >$(PROG)_min.js "`cat preamble.js`"

clean:
	rm -fr *~ .*~ .\#* \#*\#

realclean: clean
	rm -f $(PROG).js $(PROG)_min.js
