// fljst: Copyright (c) 2014 Joel "val" Ward (val@circleclick.com)
function loopArr(arr,proc,acc){
    var n,d,r;
    for(n in arr){ d=arr[n];
	if (!undef(r=proc(d[0],d[1]))) acc.push(r);}
    return acc;
}
function loopItr(it,proc,acc){
    var n,d,r;
    while(!(n=it.next()).done){	d=n.value;
	if (!undef(r=proc(d[0],d[1]))) acc.push(r);}
    return acc;
}
function loop(x,proc,acc){
    if (undef(acc)) acc=[];
    if (x.next)	return loopItr(x,proc,acc);
    else        return loopArr(x,proc,acc);}
