// fljst: Copyright (c) 2014 Joel "val" Ward (val@circleclick.com)
function sendCall(url,success,method,data){
    out("start ajax call");
    var xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.onreadystatechange = function(){
	if(xhr.readyState === 4){
	    if(xhr.status !== 200){
		out('Error: '+xhr.status);
		return;
	    }else try{
		var j =	JSON.parse(xhr.responseText);
		try{
		    success(j);
		}catch(e){
		    out("processing error");
		    return;
		}
	    }catch(e){
		out("parse error");
		return;
	    }
	    out("processed");
	}}
    xhr.send((method=="POST")?str(data):null);
    out("started ajax call");
}
function sendPost(url,success,data){
    return sendCall(url,success,"POST",data);
}
function sendGet(url,success){
    return sendCall(url,success,"GET");
}
