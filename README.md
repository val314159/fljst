fljst: a Fast, Light JavaScript Toolkit
====

Copyright (c) 2014 Joel "val" Ward (val@circleclick.com)

##Platforms:

 Fairly Recent Browsers

##Goals/Motivation

###Framework Agnosticity
   Over the years, I have rewritten these same low-level utilities over and over in one form or another.  While many framework and utility packages exist, they all come with a price.

###Package problems:
 - dependencies/conflicts
 - all or nothing
 - quicksand/too far from zero
